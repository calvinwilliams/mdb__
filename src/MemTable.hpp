#ifndef _MEMTABLE_HPP_
#define _MEMTABLE_HPP_

#include "MemDatabase.hpp"

#include <stdarg.h>

class MemDatabase ;
class MemIndex ;
enum class MemIndexType ;
class MemDataPage ;

class MemTable
{
private :
	MemDatabase			*db ;
	
	string				table_name ;
	size_t				record_size ;
	
	uint64_t			max_record_no ;
	
	list<struct MemDataPage*>	record_data_pages ;
	
	unordered_map<string,MemIndex*>	indexs ;

	struct MemIndex *QueryExecuteSchedule( struct MemField *where_field_array , size_t where_field_array_count );
	bool TestRecordMatched( char *record_data , struct MemField *where_field_array , size_t where_field_array_count , void *record_buffer );
	
public :
	MemTable( MemDatabase *db , string table_name , size_t record_size );
	~MemTable();
	
	string GetTableName();
	size_t GetRecordSize();
	size_t GetMaxRecordNo();
	
	char *QueryRecordWithoutIndex( void *record_buffer , struct MemField *where_field_array , size_t where_field_array_count );
	
	/*
	CREATE INDEX index_name ON table_name ( field1_name , field2_name ) ;
	|
	CreateIndex( tbl
		, MemIndexType::MDB_UNIQUEINDEX
		, MDBFIELD(struct_name,struct_variable1_name)
		, MDBFIELD(struct_name,struct_variable2_name)
		, MDBFIELD_NONE
		);
	*/
	MemIndex *CreateIndex( string index_name , MemIndexType index_type , ... );
	bool DropIndex( string index_name );
	bool DropIndex( MemIndex *index );
	MemIndex *QueryIndex( string index_name );
	
	/*
	INSERT INTO table_name ( field1_value , field2_value ) ;
	|
	Insert( & struct_variable );
	*/
	bool Insert( void *record_buffer );
	
	/*
	SELECT field1,field2 FROM table_name WHERE field3_name=field3_value AND field4_name=field4_value ;
	|
	Select( , & struct_variable
		, MDBFIELD(struct_name,struct_variable1_name)
		, MDBFIELD(struct_name,struct_variable2_name)
		, MDBFIELD_NONE
		, MDBFIELD(struct_name,struct_variable3_name)
		, MDBFIELD(struct_name,struct_variable4_name)
		, MDBFIELD_NONE
		);
	*/
	bool Select( void *record_buffer , ... );

} ;

struct MemRecordDataUnitHead
{
	uint64_t		record_no ;
} ;

struct MemRecordDataUnitTail
{
	struct MemDataPage	*record_datapage ;
	uint64_t		insert_id ;
	uint64_t		delete_id ;
} ;

#endif

