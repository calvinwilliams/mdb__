#ifndef _MEMINDEX_HPP_
#define _MEMINDEX_HPP_

#include "MemDatabase.hpp"
#include "MemField.hpp"

#include <stdarg.h>

#define MDB_CREATE_INDEX_FIELD_ARRAY_MAXCOUNT	16

class MemDatabase ;
class MemDataPage ;
enum class MemIndexType ;

class MemIndex
{
private :
	MemDatabase		*db ;
	
	string			index_name ;
	MemIndexType		index_type ;
	
	struct MemField		index_field_array[ MDB_CREATE_INDEX_FIELD_ARRAY_MAXCOUNT ] ;
	size_t			index_field_array_count ;
	
	list<MemDataPage*>	index_data_pages ;

	struct rb_root		index_dataunit_tree ;
	
public :
	MemIndex( MemDatabase *db , string index_name , MemIndexType index_type );
	bool Init( va_list valist );
	~MemIndex();
	
	string GetIndexName();
	MemIndexType GetIndexType();
	
	struct MemField *GetIndexFieldArray();
	size_t GetIndexFieldArrayCount();
	
	bool MaintentIndexOnInsert( char *record_dataunit );
	
	bool AddIndexDataUnitTreeNode( char *record_dataunit , struct MemIndexDataUnit *index_dataunit );
	struct MemIndexDataUnit *QueryIndexDataUnitTreeNode( void *record_buffer );
} ;

struct MemIndexDataUnit
{
	struct rb_node		index_dataunit_tree_node ;
	struct MemDataPage	*index_datapage ;
	char			*record_dataunit ;
} ;

#endif

