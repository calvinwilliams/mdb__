#ifndef _MEMDATABASEUTIL_HPP_
#define _MEMDATABASEUTIL_HPP_

#include "MemDatabase.hpp"

#include <string.h>

#ifndef offsetof
#define offsetof(_TYPE_,_MEMBER_)				((size_t)&((_TYPE_*)0)->_MEMBER_)
#endif

#define sizeof0(_STRUCT_NAME_,_STRUCT_PROPERTY_NAME_)	sizeof(((_STRUCT_NAME_*)0)->_STRUCT_PROPERTY_NAME_)

class MemDatabaseUtil
{
public :
	static bool ConvertArgsToArray( va_list valist , struct MemField *field_array , size_t field_array_size , size_t *p_field_array_count );
	static void printhex( char *buf , size_t buf_len );
} ;

#endif

