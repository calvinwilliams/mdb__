#ifndef _MEMFIELD_HPP_
#define _MEMFIELD_HPP_

#define MDBFIELD(_struct_name_,_struct_property_name_)	#_struct_property_name_ , (int)offsetof(_struct_name_,_struct_property_name_) , sizeof0(_struct_name_,_struct_property_name_)
#define MDBFIELD_NONE					NULL

struct MemField
{
	char		*field_name ;
	int		field_offset ;
	size_t		field_size ;
} ;

#endif

