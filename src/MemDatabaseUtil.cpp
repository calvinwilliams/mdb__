#include "MemDatabaseUtil.hpp"

bool MemDatabaseUtil::ConvertArgsToArray( va_list valist , struct MemField *field_array , size_t field_array_size , size_t *p_field_array_count )
{
	char		*field_name = NULL ;
	size_t		field_array_count ;
	
	memset( field_array , 0x00 , sizeof(struct MemField)*field_array_size );
	field_array_count = 0 ;
	while(1)
	{
		if( field_array_count >= field_array_size )
			return false;
		
		field_name = va_arg( valist , char* ) ;
		if( field_name == MDBFIELD_NONE )
			break;
		field_array[field_array_count].field_name = field_name ;
		field_array[field_array_count].field_offset = va_arg( valist , int ) ;
		field_array[field_array_count].field_size = va_arg( valist , size_t ) ;
#if MDB_TRACE
		cout << "TRACE - MemDatabaseUtil::ConvertArgsToArray : .field_name[" << field_array[field_array_count].field_name << "] .field_offset[" << field_array[field_array_count].field_offset << "] .field_size[" << field_array[field_array_count].field_size << "]" << endl ;
#endif
		field_array_count++;
	}
	
	if( p_field_array_count )
		(*p_field_array_count) = field_array_count ;
	
	return true;
}

void MemDatabaseUtil::printhex( char *buf , size_t buf_len )
{
	size_t		i ;
	
	cout << "[" ;
	for( i = 0 ; i < buf_len ; i++ )
	{
		printf( "0x%X" , (unsigned char)(buf[i]) );
		if( i + 1 < buf_len )
			cout << " " ;
	}
	cout << "]" ;
	
	return;
}

