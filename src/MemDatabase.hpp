#ifndef _MEMDATABASE_HPP_
#define _MEMDATABASE_HPP_

#include <iostream>
#include <string>
#include <list>
#include <unordered_map>
using namespace std ;

#include "rbtree.h"

#include "MemDatabaseUtil.hpp"
#include "MemDatabaseError.hpp"
#include "MemDataPage.hpp"
#include "MemTable.hpp"
#include "MemIndexType.hpp"
#include "MemIndex.hpp"
#include "MemField.hpp"

#define MDB_TRACE		0

#define MDB_SELECT_FIELD_ARRAY_MAXCOUNT		16
#define MDB_SET_INDEX_FIELD_ARRAY_MAXCOUNT	16
#define MDB_WHERE_INDEX_FIELD_ARRAY_MAXCOUNT	16

class MemTable ;

class MemDatabase
{
private :
	MemDatabaseError		last_error ;
	
	unordered_map<string,MemTable*>	tables ;

public :
	MemDatabase();
	~MemDatabase();
	
	MemDatabaseError GetLastError();
	int GetLastErrorNumber();
	void SetError( MemDatabaseError error );
	
	MemTable *CreateTable( string table_name , size_t record_size );
	bool DropTable( string table_name );
	bool DropTable( MemTable *table );
	MemTable *QueryTable( string table_name );
} ;

#endif

