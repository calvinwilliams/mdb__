#ifndef _MEMDATAPAGE_HPP_
#define _MEMDATAPAGE_HPP_

#include "MemDatabase.hpp"

struct MemDataPage
{
	void		*subject_to_object ;
	
	size_t		dataunit_size ;
	size_t		dataunit_prealloc_count ;
	size_t		dataunit_used_count ;
	uint64_t	dataunit_used_bitmap ;
	char		dataunit_array_base[0] ;
} ;

struct MemDataPage *CreateDataPage( void *subject_to_object , size_t dataunit_size );
void DestroyDataPage( struct MemDataPage *datapage );

size_t GetDataUnitSize( struct MemDataPage *datapage );
size_t GetPreAllocCount( struct MemDataPage *datapage );
size_t GetUsedCount( struct MemDataPage *datapage );
bool IsEmpty( struct MemDataPage *datapage );
bool IsFull( struct MemDataPage *datapage );

char *GetUnusedDataUnit( struct MemDataPage *datapage );
void SetDataUnitUnused( struct MemDataPage *datapage , char *dataunit );

#endif

