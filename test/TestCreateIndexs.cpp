#include "MemDatabase.hpp"

#include "UserInfo.hpp"

int TestCreateIndexs()
{
	MemDatabase		*db = NULL ;
	MemTable		*table = NULL ;
	MemIndex		*index = NULL ;
	bool			bret ;
	
	cout << "create database ..." << endl ;
	db = new MemDatabase ;
	if( db == NULL )
	{
		cout << "ERROR : create database failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create database ok" << endl ;
	}
	
	cout << "create table mytable ..." << endl ;
	table = db->CreateTable( "mytable" , sizeof(UserInfo) ) ;
	if( table == NULL )
	{
		cout << "ERROR : create table mytable failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create table mytable ok" << endl ;
	}
	
	cout << "create index myindex ..." << endl ;
	index = table->CreateIndex( "myindex" , MemIndexType::MDB_UNIQUEINDEX , MDBFIELD(struct UserInfo,uid) , MDBFIELD_NONE ) ;
	if( index == NULL )
	{
		cout << "ERROR : create index myindex failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create index myindex ok" << endl ;
	}
	
	cout << "create index myindex2 ..." << endl ;
	index = table->CreateIndex( "myindex2" , MemIndexType::MDB_UNIQUEINDEX , MDBFIELD(struct UserInfo,user_name) , MDBFIELD(struct UserInfo,email) , MDBFIELD_NONE ) ;
	if( index == NULL )
	{
		cout << "ERROR : create index myindex2 failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create index myindex2 ok" << endl ;
	}
	
	cout << "create index myindex3 ..." << endl ;
	index = table->CreateIndex( "myindex3" , MemIndexType::MDB_INDEX , MDBFIELD(struct UserInfo,email) , MDBFIELD_NONE ) ;
	if( index == NULL )
	{
		cout << "ERROR : create index myindex3 failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create index myindex3 ok" << endl ;
	}
	
	cout << "drop index myindex ..." << endl ;
	bret = table->DropIndex( "myindex" ) ;
	if( bret != true )
	{
		cout << "ERROR : drop index myindex failed[" << db->GetLastErrorNumber() << "]" << endl ;
		return -1;
	}
	else
	{
		cout << "drop index myindex ok" << endl ;
	}
	
	cout << "drop index myindex3 ..." << endl ;
	bret = table->DropIndex( index ) ;
	if( bret != true )
	{
		cout << "ERROR : drop index myindex3 failed[" << db->GetLastErrorNumber() << "]" << endl ;
		return -1;
	}
	else
	{
		cout << "drop index myindex3 ok" << endl ;
	}
	
	cout << "drop database ..." << endl ;
	delete db ;
	cout << "drop database ok" << endl ;
	
	return 0;
}

int main()
{
	return -TestCreateIndexs();
}

