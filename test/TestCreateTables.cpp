#include "MemDatabase.hpp"

#include "UserInfo.hpp"

int TestCreateTables()
{
	MemDatabase		*db = NULL ;
	MemTable		*table = NULL ;
	bool			bret ;
	
	cout << "create database ..." << endl ;
	db = new MemDatabase ;
	if( db == NULL )
	{
		cout << "ERROR : create database failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create database ok" << endl ;
	}
	
	cout << "create table mytable ..." << endl ;
	table = db->CreateTable( "mytable" , sizeof(UserInfo) ) ;
	if( table == NULL )
	{
		cout << "ERROR : create table mytable failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create table mytable ok" << endl ;
	}
	
	cout << "create table mytable2 ..." << endl ;
	table = db->CreateTable( "mytable2" , sizeof(UserInfo) ) ;
	if( table == NULL )
	{
		cout << "ERROR : create table mytable2 failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create table mytable2 ok" << endl ;
	}
	
	cout << "create table mytable3 ..." << endl ;
	table = db->CreateTable( "mytable3" , sizeof(UserInfo) ) ;
	if( table == NULL )
	{
		cout << "ERROR : create table mytable3 failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create table mytable3 ok" << endl ;
	}
	
	cout << "drop table mytable ..." << endl ;
	bret = db->DropTable( "mytable" ) ;
	if( bret != true )
	{
		cout << "ERROR : drop table mytable failed[" << db->GetLastErrorNumber() << "]" << endl ;
		return -1;
	}
	else
	{
		cout << "drop table mytable ok" << endl ;
	}
	
	cout << "drop table mytable3 ..." << endl ;
	bret = db->DropTable( table ) ;
	if( bret != true )
	{
		cout << "ERROR : drop table mytable3 failed[" << db->GetLastErrorNumber() << "]" << endl ;
		return -1;
	}
	else
	{
		cout << "drop table mytable3 ok" << endl ;
	}
	
	cout << "drop database ..." << endl ;
	delete db ;
	cout << "drop database ok" << endl ;
	
	return 0;
}

int main()
{
	return -TestCreateTables();
}

