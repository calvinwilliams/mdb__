struct UserInfo
{
	size_t		uid ;
	char		user_name[ 32 ] ;
	char		email[ 128 ] ;
} ;

#define BEGIN_TIMING(_tv_)	gettimeofday( & (_tv_) , NULL );
#define END_TIMING(_tv_)	{ \
					struct timeval	end ; \
					gettimeofday( & end , NULL ); \
					(_tv_).tv_sec = end.tv_sec - (_tv_).tv_sec ; \
					(_tv_).tv_usec = end.tv_usec - (_tv_).tv_usec ; \
					if( (_tv_).tv_usec < 0 ) \
					{ \
						(_tv_).tv_sec--; \
						(_tv_).tv_usec += 1000000 ; \
					} \
				} \

