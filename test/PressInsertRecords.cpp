#include "MemDatabase.hpp"

#include "UserInfo.hpp"

int PressInsertRecords( size_t press_count )
{
	MemDatabase		*db = NULL ;
	MemTable		*table = NULL ;
	MemIndex		*index = NULL ;
	struct UserInfo		user_info ;
	bool			bret ;
	
	cout << "create database ..." << endl ;
	db = new MemDatabase ;
	if( db == NULL )
	{
		cout << "ERROR : create database failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create database ok" << endl ;
	}
	
	cout << "create table mytable ..." << endl ;
	table = db->CreateTable( "mytable" , sizeof(UserInfo) ) ;
	if( table == NULL )
	{
		cout << "ERROR : create table mytable failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create table mytable ok" << endl ;
	}
	
	cout << "create index myindex ..." << endl ;
	index = table->CreateIndex( "myindex" , MemIndexType::MDB_UNIQUEINDEX , MDBFIELD(struct UserInfo,uid) , MDBFIELD_NONE ) ;
	if( index == NULL )
	{
		cout << "ERROR : create index myindex failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create index myindex ok" << endl ;
	}
	
	cout << "table->Insert ..." << endl ;
	memset( & user_info , 0x00 , sizeof(struct UserInfo) );
	for( user_info.uid = 1 ; user_info.uid <= press_count ; user_info.uid++ )
	{
		snprintf( user_info.user_name , sizeof(user_info.user_name)-1 , "myname-%zu" , user_info.uid );
		snprintf( user_info.email , sizeof(user_info.email)-1 , "myname-%zu@163.com" , user_info.uid );
		bret = table->Insert( & user_info ) ;
		if( bret != true )
		{
			cout << "table->Insert uid[" << user_info.uid << "] user_name[" << user_info.user_name << "] email[" << user_info.email << "] failed" << endl ;
			return -1;
		}
		
		// cout << "table->Insert uid[" << user_info.uid << "] user_name[" << user_info.user_name << "] email[" << user_info.email << "] ok" << endl ;
	}
	cout << "table->Insert all ok , press_count[" << press_count << "]" << endl ;
	
	cout << "drop database ..." << endl ;
	delete db ;
	cout << "drop database ok" << endl ;
	
	return 0;
}

int main( int argc , char *argv[] )
{
	if( argc != 1 + 1 )
	{
		cout << "USAGE : press_insert_records press_count" << endl ;
		exit(1);
	}
	
	return -PressInsertRecords( (size_t)atoi(argv[1]) );
}

