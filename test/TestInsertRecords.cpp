#include "MemDatabase.hpp"

#include "UserInfo.hpp"

int TestInsertRecords()
{
	MemDatabase		*db = NULL ;
	MemTable		*table = NULL ;
	MemIndex		*index = NULL ;
	struct UserInfo		user_info ;
	bool			bret ;
	
	cout << "create database ..." << endl ;
	db = new MemDatabase ;
	if( db == NULL )
	{
		cout << "ERROR : create database failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create database ok" << endl ;
	}
	
	cout << "create table mytable ..." << endl ;
	table = db->CreateTable( "mytable" , sizeof(UserInfo) ) ;
	if( table == NULL )
	{
		cout << "ERROR : create table mytable failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create table mytable ok" << endl ;
	}
	
	cout << "create index myindex ..." << endl ;
	index = table->CreateIndex( "myindex" , MemIndexType::MDB_UNIQUEINDEX , MDBFIELD(struct UserInfo,uid) , MDBFIELD_NONE ) ;
	if( index == NULL )
	{
		cout << "ERROR : create index myindex failed" << endl ;
		return -1;
	}
	else
	{
		cout << "create index myindex ok" << endl ;
	}
	
	memset( & user_info , 0x00 , sizeof(struct UserInfo) );
	for( user_info.uid = 1 ; user_info.uid <= 5 ; user_info.uid++ )
	{
		snprintf( user_info.user_name , sizeof(user_info.user_name)-1 , "myname-%zu" , user_info.uid );
		snprintf( user_info.email , sizeof(user_info.email)-1 , "myname-%zu@163.com" , user_info.uid );
		cout << "table->Insert uid[" << user_info.uid << "] user_name[" << user_info.user_name << "] email[" << user_info.email << "] ..." << endl ;
		bret = table->Insert( & user_info ) ;
		if( bret != true )
		{
			cout << "table->Insert uid[" << user_info.uid << "] user_name[" << user_info.user_name << "] email[" << user_info.email << "] failed" << endl ;
			return -1;
		}
		else
		{
			cout << "table->Insert uid[" << user_info.uid << "] user_name[" << user_info.user_name << "] email[" << user_info.email << "] ok" << endl ;
		}
	}
	printf( "table->Insert all ok\n" );
	
	memset( & user_info , 0x00 , sizeof(struct UserInfo) );
	user_info.uid = 1 ;
	cout << "table->Select uid[" << user_info.uid << "] ..." << endl ;
	bret = table->Select( & user_info , MDBFIELD(struct UserInfo,user_name) , MDBFIELD(struct UserInfo,email) , MDBFIELD_NONE , MDBFIELD(struct UserInfo,uid) , MDBFIELD_NONE ) ;
	if( bret != true )
	{
		cout << "ERROR : table->Select failed" << endl ;
		return -1;
	}
	else
	{
		cout << "table->Select ok , uid[" << user_info.uid << "] user_name[" << user_info.user_name << "] email[" << user_info.email << "] ..." << endl ;
	}
	
	memset( & user_info , 0x00 , sizeof(struct UserInfo) );
	user_info.uid = 9 ;
	cout << "table->Select uid[" << user_info.uid << "] ..." << endl ;
	bret = table->Select( & user_info , MDBFIELD(struct UserInfo,user_name) , MDBFIELD(struct UserInfo,email) , MDBFIELD_NONE , MDBFIELD(struct UserInfo,uid) , MDBFIELD_NONE ) ;
	if( bret != true )
	{
		cout << "table->Select failed" << endl ;
	}
	else
	{
		cout << "ERROR : table->Select ok , uid[" << user_info.uid << "] user_name[" << user_info.user_name << "] email[" << user_info.email << "] ..." << endl ;
		return -1;
	}
	
	memset( & user_info , 0x00 , sizeof(struct UserInfo) );
	strcpy( user_info.email , "myname-3@163.com" );
	cout << "table->Select email[" << user_info.email << "] ..." << endl ;
	bret = table->Select( & user_info , MDBFIELD(struct UserInfo,user_name) , MDBFIELD(struct UserInfo,email) , MDBFIELD_NONE , MDBFIELD(struct UserInfo,email) , MDBFIELD_NONE ) ;
	if( bret != true )
	{
		cout << "ERROR : table->Select failed" << endl ;
		return -1;
	}
	else
	{
		cout << "table->Select ok , uid[" << user_info.uid << "] user_name[" << user_info.user_name << "] email[" << user_info.email << "] ..." << endl ;
	}
	
	memset( & user_info , 0x00 , sizeof(struct UserInfo) );
	strcpy( user_info.email , "myname-9@163.com" );
	cout << "table->Select email[" << user_info.email << "] ..." << endl ;
	bret = table->Select( & user_info , MDBFIELD(struct UserInfo,user_name) , MDBFIELD(struct UserInfo,email) , MDBFIELD_NONE , MDBFIELD(struct UserInfo,email) , MDBFIELD_NONE ) ;
	if( bret != true )
	{
		cout << "table->Select failed" << endl ;
	}
	else
	{
		cout << "ERROR : table->Select ok , uid[" << user_info.uid << "] user_name[" << user_info.user_name << "] email[" << user_info.email << "] ..." << endl ;
		return -1;
	}
	
	cout << "drop database ..." << endl ;
	delete db ;
	cout << "drop database ok" << endl ;
	
	return 0;
}

int main()
{
	return -TestInsertRecords();
}

